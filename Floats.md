Some ref:

Pole Float Shotting Patterns Explained | Deck Fishing: https://www.youtube.com/watch?v=AScPWUlsWXM

Pole Float Shotting Patterns Explained | Maggot Fishing: https://www.youtube.com/watch?v=1Uo9OTurL_o


**Size**

- 0.1g per 1.5 foot of water fishing in
- 0.2g will be for 3ft of water
- 0.4g to 0.6g for 6ft of water
- On still water above applies, on windy days or on river you need heavier

**Weights**
Floats in bold should be 80% of what you have. Need larger for fast water or lighter for shallow.

_common weights:_
- 3x10 = 0.10g (2 x No10 shot)
- 4x10 = 0.15g (3 x No9 shot) (jamie H uses all no 11)
- 4x12 = 0.2g (5x No10 shot) 2.5ft to 3.5ft (jamie H uses all no 11)
- 4x14 = 0.4g (6 x No8 shot) 3.5ft to 4.5ft
- 4x16 = 0.5g (8x No8 shot) 4.5ft to 6.5ft (jamie H uses all no 8)
- 4x18 = 0.75g (3 x No3 shot)
- 4x20 = 1g (4 x No3 shot)
- 5x20 = 1.25g (5 x No3 shot)
- 6x20 = 1.5g (6 x No3 shot)

_Shot sizes:_
- 3SSG = 4.8g
- 2SSG = 3.2g
- LG = 3g
- LSG = 2g
- SSG = 1.6g
- AAA = 0.8g
- AB = 0.6g
- BB = 0.4g
- No1 = 0.3g
- No3 = 0.25g
- No4 = 0.2g
- No5 = 0.15g
- No6 = 0.1g
- No8 = 0.06g
- No9 = 0.05g
- No10 = 0.04g
- No11 = 0.03g
- No12 = 0.02g
- No13 = 0.01g

**Shapes**

- Slim = on the drop fishing
- Rugby Ball = on the drop, more stability for larger baits
- Round = for rivers
- Diamond = fishing on the bottom
- Dibber = shallow light fishing
- Body down/short = shallow fishing with heavy baits


**Stem**
- Carbon, light for fishing on the drop
- Wire, fishing on the bottom and for stability in wind
- Fibreglass, strong and light for larger fish.

**Bristle**
Use small bristle for small baits e.g. 1 maggot 1.2mm and 2mm for 4mm pellet or sweetcorn.

- Solid, hard to see but more sensitive
- Hollow, easier to see for larger baits
- 1.2mm to 1.5mm for silvers
- 1.5mm to 1.8mm for larger fish
- 2mm to 2.5mm for carp

**Bulk**

Use bulk to get bait to bottom quickly.

Bulk made up of main weight, upto 5ft bulk then 2 droppers before hooklength. 5ft to 7ft bulk then 3 droppers. Over 7ft 4 droppers.

Droppers should be 12/13 shots.
