**Line**

https://www.anglingdirect.co.uk/preston-reflo-powerline

**Silvers smaller fish**

Mainline: 0.13

Hooklengths:

0.7, 0.8, 0.10 canal

0.10, 0.11 commercial winter

**Carp larger fish**

Mainline: 0.17

Hooklength: 0.15

**Margins**

Mainline: 0.20

Hooklength: 0.17
